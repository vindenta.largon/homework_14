﻿#include <iostream>
#include <cmath>

class Coub
{
private:
	int a, b, c;
	
public:
	Coub() : a(10), b(10), c(10)
	{}
	void ShowData()
	{
		std::cout << "Dimensions: " << a << "x" << b << "x" << c << std::endl;
	}
};

class Vector
{
private:
	double x, y, z, SizeVector;

public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void size(double _x, double _y, double _z)
	{
		x = _x;
		y = _y;
		z = _z;
		SizeVector = sqrt(x*x+y*y+z*z);
		std::cout << "Size: " << SizeVector << std::endl;
	}
};

int main()
{
	Coub one;
	one.ShowData();
	Vector front;
	front.size(11, -12, -5);
}

